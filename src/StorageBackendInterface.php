<?php

namespace Drupal\events_logging;

/**
 * Interface for storage backend.
 */
interface StorageBackendInterface {

  /**
   * Save data to storage.
   *
   * @param array $data
   *   The data to be saved.
   *
   * @return mixed
   *   The save status.
   */
  public function save($data);

  /**
   * Delete all records.
   */
  public function deleteAll();

  /**
   * Delete a selected log record.
   *
   * @param array $data
   *   The data to be deleted.
   */
  public function delete($data);

}
