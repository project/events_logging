<?php

namespace Drupal\events_logging;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Psr\Log\LoggerInterface as DrupalLogger;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class for logging events to storage.
 */
class Logger implements LoggerInterface {

  /**
   * The storage backend plugin manager.
   *
   * @var \Drupal\events_logging\StorageBackendPluginManagerInterface
   */
  private $storageBackendPluginManager;

  /**
   * The Drupal logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private $drupalLogger;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $config;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The user account service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * Logger constructor.
   *
   * @param \Drupal\events_logging\StorageBackendPluginManagerInterface $storage_backend_plugin_manager
   *   The storage backend plugin manager.
   * @param \Psr\Log\LoggerInterface $drupal_logger
   *   The Drupal logger.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   The request stack.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account service.
   */
  public function __construct(
    StorageBackendPluginManagerInterface $storage_backend_plugin_manager,
    DrupalLogger $drupal_logger,
    ConfigFactoryInterface $config,
    EntityTypeManagerInterface $entity_type_manager,
    RequestStack $request,
    AccountInterface $account
  ) {
    $this->storageBackendPluginManager = $storage_backend_plugin_manager;
    $this->drupalLogger = $drupal_logger;
    $this->config = $config;
    $this->entityTypeManager = $entity_type_manager;
    $this->request = $request;
    $this->currentUser = $account;
  }

  /**
   * Log data to the backend.
   *
   * @param array $data
   *   The data to be logged.
   */
  public function log($data) {
    // phpcs:disable
    // Uncomment this once the storage_backend_id is configurable.
    // $storageBackendId = $this->config->get('events_logging.settings')->get('storage_backend_id');
    // phpcs:enable

    try {
      /** @var \Drupal\events_logging\StorageBackendInterface $storageBackend */
      $storageBackend = $this->storageBackendPluginManager->createInstance('database');
      $storageBackend->save($data);
    }
    catch (PluginException $e) {
      $this->drupalLogger->error("Errors in logging data %data: %error", [
        print_r($data, TRUE),
        $e->getMessage(),
      ]);
    }
  }

  /**
   * Check if the entity is enabled.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The log entity.
   *
   * @return bool
   *   Return TRUE if the entity is enabled, else FALSE.
   */
  public function checkIfEntityIsEnabled(EntityInterface $entity) {
    $entity_type_id = $entity->getEntityType()->id();
    $events_logging_config = $this->config->get('events_logging.config');
    $events_logging_content_entities = $events_logging_config->get('enabled_content_entities') ?: [];
    $events_logging_config_entities = $events_logging_config->get('enabled_config_entities') ?: [];
    if ($entity_type_id == 'events_logging') {
      return FALSE;
    }
    if (in_array($entity_type_id, $events_logging_content_entities) || in_array($entity_type_id, $events_logging_config_entities)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Save log to entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The log entity.
   * @param string $type
   *   The log entry type.
   * @param string $description
   *   The log description.
   */
  public function createLogEntity(EntityInterface $entity, $type, $description = NULL) {
    $values = [];
    $values['type'][0]['value'] = $entity->getEntityType()->id() . '_' . $type;
    $values['operation'][0]['value'] = $type;
    $values['logpath'][0]['value'] = $this->request->getCurrentRequest()->getRequestUri();
    $values['ref_numeric'][0]['value'] = $entity->id();
    $values['name'][0]['value'] = $entity->label();
    $values['ref_title'][0]['value'] = $entity->label();
    $values['description'][0]['value'] = $description ?: $this->getLogDescription($entity, $type);
    $values['ip'][0]['value'] = $this->request->getCurrentRequest()->getClientIp();

    $events_logging_storage = $this->entityTypeManager->getStorage('events_logging');
    $events_logging_entity = $events_logging_storage->create($values);

    try {
      $events_logging_entity->save();
    }
    catch (EntityStorageException $e) {
      $this->drupalLogger->error("Errors in saving entity %data: %error", [
        '%data' => print_r($values, TRUE),
        '%error' => $e->getMessage(),
      ]);
    }
  }

  /**
   * Get the custom description.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The event log entity.
   * @param string $type
   *   The operations type.
   *
   * @return string|false
   *   The custom configuration if any.
   */
  public function getCustomDescription($entity, $type) {
    $config = $this->config->get('events_logging.config');
    $custom_description = $config->get('events_logging_description');

    if (!empty($custom_description)) {
      $data = [
        'entity' => $entity,
        'operation' => $type,
      ];
      $token_service = \Drupal::token();
      return $token_service->replace($custom_description, $data);
    }

    return FALSE;
  }

  /**
   * Get the log's description field.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The log entity.
   * @param string $type
   *   The operations type.
   *
   * @return string
   *   The log's description.
   */
  protected function getLogDescription(EntityInterface $entity, $type) {
    $name = $this->currentUser->getAccountName();
    $uid = $this->currentUser->id();
    $entname = $entity->getEntityType()->getLabel();
    $entid = $entity->id();
    $description = "user $name (uid $uid) performed $type operation on entity $entname (id $entid)";
    return $description;
  }

  /**
   * Remove old logs from storage.
   */
  public function purgeOldLogs() {
    $config = $this->config->get('events_logging.config');
    $max_records = $config->get('max_records');
    if ($max_records) {
      $events_logging_storage = $this->entityTypeManager->getStorage('events_logging');
      $query = $events_logging_storage->getQuery();
      $query->sort('created', 'DESC');
      $query->accessCheck(TRUE);
      $results = $query->execute();

      if (!empty($results)) {
        $delete_chunks = array_chunk(array_slice($results, $max_records), 50);
        foreach ($delete_chunks as $delete_ids) {
          $delete_records = $events_logging_storage->loadMultiple($delete_ids);
          try {
            $events_logging_storage->delete($delete_records);
          }
          catch (EntityStorageException $e) {
            $this->drupalLogger->error("Errors in deleting rows %rows: %error", [
              '%rows' => print_r($delete_records, TRUE),
              '%error' => $e->getMessage(),
            ]);
          }
        }
      }
    }
  }

}
