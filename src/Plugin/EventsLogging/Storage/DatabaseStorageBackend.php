<?php

namespace Drupal\events_logging\Plugin\EventLog\Storage;

use Drupal\events_logging\StorageBackendInterface;

/**
 * Class for database storage backend.
 *
 * @StorageBackend(
 *   id = "database",
 *   label = @Translation("Database"),
 *   description = @Translation("Store event logs in the database.")
 * )
 */
class DatabaseStorageBackend implements StorageBackendInterface {

  /**
   * {@inheritdoc}
   */
  public function save($data) {
    // Placeholder method.
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAll() {
    // Placeholder method.
  }

  /**
   * {@inheritdoc}
   */
  public function delete($data) {
    // Placeholder method.
  }

}
