<?php

namespace Drupal\events_logging\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a StorageBackend annotation object.
 *
 * @Annotation
 */
class StorageBackend extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the StorageBackend type.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A short description of the StorageBackend type.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
