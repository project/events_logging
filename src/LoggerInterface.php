<?php

namespace Drupal\events_logging;

use Drupal\Core\Entity\EntityInterface;

/**
 * Interface for logger.
 */
interface LoggerInterface {

  /**
   * Log the data to storage.
   *
   * @param array $data
   *   The data to be logged.
   */
  public function log($data);

  /**
   * Check if the log entity is enabled.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The log entity.
   */
  public function checkIfEntityIsEnabled(EntityInterface $entity);

  /**
   * Create the log entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The log entity.
   * @param string $type
   *   The event type.
   */
  public function createLogEntity(EntityInterface $entity, $type);

}
