<?php

namespace Drupal\events_logging\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\events_logging\StorageBackendPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class for configurations form.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logging storage manager.
   *
   * @var \Drupal\events_logging\StorageBackendPluginManager
   */
  protected $loggingStorage;

  /**
   * Constructs a \Drupal\ajax_comments\Form\SettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\events_logging\StorageBackendPluginManager $logging_storage
   *   The logging storage manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, StorageBackendPluginManager $logging_storage) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->loggingStorage = $logging_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.events_logging_storage_backend')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'events_logging.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('events_logging.config');
    $potential_entities = $this->getPotentialEntities();
    $form['enabled_content_entities'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled content entities'),
      '#description' => $this->t('Choose the content entities you want to log'),
      '#options' => $potential_entities['content_entities'],
      '#default_value' => $config->get('enabled_content_entities') ?: [],
    ];
    $form['enabled_config_entities'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled config entities'),
      '#description' => $this->t('Choose the config entities you want to log'),
      '#options' => $potential_entities['config_entities'],
      '#default_value' => $config->get('enabled_config_entities') ?: [],
    ];
    $form['enabled_events_logging_plugins'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled events logging plugins'),
      '#description' => $this->t('Choose the events logging plugin'),
      '#options' => $this->getPotentialPlugins(),
      '#default_value' => $config->get('enabled_events_logging_plugins') ?: [],
    ];
    $form['events_logging_max_records'] = [
      '#type' => 'select',
      '#title' => $this->t('Eventlog messages to keep'),
      '#description' => $this->t('The maximum number of messages to keep in the database event log. Requires a cron maintenance task.'),
      '#options' => [
        0 => $this->t('All'),
        1000 => '1000',
        2500 => '2500',
        5000 => '5000',
        10000 => '10000',
        25000 => '25000',
        50000 => '50000',
        100000 => '100000',
        250000 => '250000',
        500000 => '500000',
        1000000 => '1000000',
      ],
      '#default_value' => $config->get('max_records') ?: 0,
      '#required' => FALSE,
    ];
    $form['events_logging_description_fieldset'] = [
      '#type' => 'fieldset',
      '#collapsed' => TRUE,
      '#collapsible' => FALSE,
    ];
    $form['events_logging_description_fieldset']['events_logging_description'] = [
      '#type' => 'textarea',
      '#title' => t('Eventlog messages description template'),
      '#description' => t('The custom template that will be used when events are logged.'),
      '#prefix' => '<div id="events-logging-description-wrapper">',
      '#suffix' => '</div>',
      '#default_value' => $config->get('events_logging_description') ? $config->get('events_logging_description') : '',
      '#required' => FALSE,
    ];
    $form['events_logging_description_fieldset']['token_browser'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => 'all',
      '#click_insert' => FALSE,
      '#dialog' => TRUE,
    ];
    $form['events_logging_description_fieldset']['events_loggin_description_reset'] = [
      '#type' => 'button',
      '#value' => t('Use default description template.'),
      '#description' => t('Reset eventlog messages description template to its default value.'),
      '#ajax' => [
        'callback' => '::descriptionResetCallback',
        'wrapper' => 'events-logging-description-wrapper',
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('events_logging.config')
      ->set('enabled_content_entities', $form_state->getValue('enabled_content_entities'))
      ->set('enabled_config_entities', $form_state->getValue('enabled_config_entities'))
      ->set('max_records', $form_state->getValue('events_logging_max_records'))
      ->set('enabled_events_logging_plugins', $form_state->getValue('enabled_events_logging_plugins'))
      ->set('events_logging_description', $form_state->getValue('events_logging_description'))
      ->save();
  }

  /**
   * Resets the description field.
   *
   * @param array $form
   *   The config form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   *   The description field settings.
   */
  public function descriptionResetCallback(array &$form, FormStateInterface $form_state) {
    $form['events_logging_description_fieldset']['events_logging_description']['#value'] = $this->getDefaultLogDescription();
    return $form['events_logging_description_fieldset']['events_logging_description'];
  }

  /**
   * Obtains potential content entities to log.
   *
   * @return array
   *   The content entity type and config entity type.
   */
  private function getPotentialEntities() {
    $content_entity_types = [];
    $config_entity_types = [];
    $entity_type_definitions = $this->entityTypeManager->getDefinitions();
    /** @var EntityTypeInterface $definition */
    foreach ($entity_type_definitions as $definition) {
      $id = $definition->id();
      if ($id == 'events_logging') {
        continue;
      }
      if ($definition instanceof ContentEntityType) {
        $content_entity_types[$id] = $definition->getLabel();
      }
      else {
        $config_entity_types[$id] = $definition->getLabel();
      }
    }
    return [
      'content_entities' => $content_entity_types,
      'config_entities' => $config_entity_types,
    ];
  }

  /**
   * Get the list of potential plugins.
   *
   * @return array
   *   The potential plugins.
   */
  private function getPotentialPlugins() {
    $definitions = [];
    $plugin_definitions = $this->loggingStorage->getDefinitions();
    foreach ($plugin_definitions as $machine_name => $definition) {
      $definitions[$machine_name] = $definition['label'];
    }
    return $definitions;
  }

  /**
   * Get the default log description.
   *
   * @return string
   *   The default log description.
   */
  private function getDefaultLogDescription() {
    $name = '[current-user:account-name]';
    $uid = '[current-user:uid]';
    $type = '[events-logging-operation:operation]';
    $entname = '[events-logging-entity:entity-name]';
    $entid = '[events-logging-entity:entity-id]';
    return "user $name (uid $uid) performed $type operation on entity $entname (id $entid)";
  }

}
